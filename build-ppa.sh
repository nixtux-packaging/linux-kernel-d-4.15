#!/bin/bash

# Dependencies:
# apt install kernel-wedge fakeroot

pkg_name="linux"

dir0="$PWD"
old_header=$(head -1 ./debian.master/changelog)

for i in bionic disco #eoan
do
	old_version="$(cat ./debian.master/changelog | head -n 1 | awk -F "(" '{print $2}' | awk -F ")" '{print $1}')"
	new_version="${old_version}~${i}1"
	sed -i -re "s/${old_version}/${new_version}/g" ./debian.master/changelog
	sed -i -re "1s/unstable/$i/" ./debian.master/changelog
	# kernel-specific commands
	rm -fv debian/changelog debian/control
	debian/rules debian/control
	fakeroot debian/rules clean
	# -I to exclude .git; -d to allow building .changes file without build dependencies installed
	dpkg-buildpackage -I -S -sa -d
	sed  -i -re "1s/.*/${old_header}/" ./debian.master/changelog
	cd ..
	
	# change PPA names to yours, you may leave only one PPA
	for reponame in "ppa:mikhailnov/linux-kernel-d2"
	do
		dput -f "$reponame" "${pkg_name}_${new_version}_source.changes" 
	done
	
	# kernel-specific clean up
	rm -fv debian/changelog debian/control debian/copyright debian/files scripts/ubuntu-retpoline-extract-one
	
	cd "$dir0"
	sleep 1
done

cd "$dir_start"
